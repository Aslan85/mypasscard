document.addEventListener("DOMContentLoaded", function ()
{
    // Grid size
    const numRows = 5; // Number of rows
    const numCols = 5; // Number of columns
    const maxChara = 2; // Number of characters max inside a bubble
    const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()";

    // control the randomness
    const seedInput = document.querySelector("#seedInput");
    const applySeedButton = document.querySelector("#applySeedButton");

    // grid and input
    const pageTitle = document.querySelector("#pageTitle");
    const gridContainer = document.querySelector(".grid-container");
    const inputField = document.querySelector("#inputField");
    let isDragging = false;
    
    // Keep track of selected characters
    let selectedCharacters = "";
    let lastTouchedBubble = null;

    // Store selected bubbles for drawing lines
    const selectedBubbles = []; 
    let lineCanvas = null;
    let ctx = null;

    // print to pdf button
    const printToPdfButton = document.querySelector("#printToPdfButton");

    // Add an input event listener to the seed input field
    seedInput.addEventListener("input", function ()
    {
        // Enable or disable the "Apply" button based on the input value
        applySeedButton.disabled = seedInput.value === "";
    });

    gridContainer.addEventListener("mousedown", startDrag);
    gridContainer.addEventListener("touchstart", startDrag);
    function startDrag(event)
    {
        event.preventDefault(); // Prevent default touch behavior
        isDragging = true;
        selectedCharacters = "";
        selectedBubbles.length = 0; // Clear previously selected bubbles
    
        if (event.target.classList.contains("bubble"))
        {
            const character = event.target.textContent;
            selectedCharacters = character;
            inputField.value = selectedCharacters;
            selectedBubbles.push(event.target);  // Store the starting bubble
    
            // Create a canvas for drawing lines
            lineCanvas = document.createElement("canvas");
            lineCanvas.classList.add("line-canvas");
            gridContainer.appendChild(lineCanvas);
            ctx = lineCanvas.getContext("2d");
        }
    }

    gridContainer.addEventListener("mouseup", endDrag);
    gridContainer.addEventListener("touchend", endDrag);
    function endDrag()
    {
        isDragging = false;
        inputField.value = selectedCharacters; // Update input field when dragging ends

        // Remove the canvas after drawing
        if (lineCanvas)
        {
            lineCanvas.remove();
            lineCanvas = null;
            ctx = null;
        }

        selectedBubbles.length = 0; // Clear selected bubbles

        // Call the copyToClipboard function
        copyToClipboard();
    }

    
    gridContainer.addEventListener("mouseover", dragging);
    gridContainer.addEventListener("touchmove", dragging);
    function dragging(event)
    {
        if (isDragging && event.target.classList.contains("bubble"))
        {
            if(lastTouchedBubble == event.target.getBoundingClientRect())
            {
                return;
            }

            lastTouchedBubble = event.target.getBoundingClientRect();
            event.preventDefault(); // Prevent scrolling while dragging
            const character = event.target.textContent;
            selectedCharacters += character;
            inputField.value = selectedCharacters;
            selectedBubbles.push(event.target);
    
            if (ctx)
            {
                drawLines(selectedBubbles);
            }
        }
    }

    applySeedButton.addEventListener("click", () =>
    {
        const seed = seedInput.value;
        Math.seedrandom(seed); // Set the seed for Math.random()

        // Clear the password input field
        inputField.value = "";

        // Remove existing bubbles
        gridContainer.innerHTML = '';
        
        // Generate bubbles using the seeded random sequence
        for (let row = 0; row < numRows; row++)
        {
            const rowElement = document.createElement("div");
            rowElement.classList.add("row");

            for (let col = 0; col < numCols; col++)
            {
                const bubble = document.createElement("div");
                bubble.classList.add("bubble");

                const randomCharacterCount = Math.floor(Math.random() * maxChara) + 1; // Generate 1 to max characters
                let bubbleContent = "";

                for (let i = 0; i < randomCharacterCount; i++) {
                    const randomIndex = Math.floor(Math.random() * characters.length);
                    bubbleContent += characters.charAt(randomIndex);
                }

                bubble.textContent = bubbleContent;

                rowElement.appendChild(bubble);
            }

            gridContainer.appendChild(rowElement);
        }

        // Randomly select a bubble and set its content to "★"
        const bubbles = document.querySelectorAll(".bubble");
        const randomBubbleIndex = Math.floor(Math.random() * bubbles.length);
        bubbles[randomBubbleIndex].textContent = "*";
        bubbles[randomBubbleIndex].classList.add("special-bubble");
    });

    // Function to draw lines between selected bubbles
    function drawLines(bubbles)
    {
        const bubbleRects = bubbles.map(bubble => bubble.getBoundingClientRect());

        lineCanvas.width = gridContainer.offsetWidth;
        lineCanvas.height = gridContainer.offsetHeight;

        ctx.clearRect(0, 0, lineCanvas.width, lineCanvas.height);

        ctx.strokeStyle = "#f65a0f"; // Line color -> https://giggster.com/guide/complementary-colors/
        ctx.lineWidth = 4; // Line widht

        ctx.beginPath();
        ctx.moveTo(bubbleRects[0].left + bubbleRects[0].width / 2, bubbleRects[0].top + bubbleRects[0].height / 2);

        for (let i = 1; i < bubbleRects.length; i++) {
            ctx.lineTo(bubbleRects[i].left + bubbleRects[i].width / 2, bubbleRects[i].top + bubbleRects[i].height / 2);
        }

        ctx.stroke();
    }

    printToPdfButton.addEventListener("click", () =>
    {
        const pdf = new jsPDF();
        const cardWidth = 54; // Width of the credit card in millimeters
        const cardHeight = 85.6; // Height of the credit card in millimeters
        const offset = 10;
        const gridAspectRatio = numRows / numCols;
    
        // Calculate the dimensions of the grid while maintaining its aspect ratio
        let gridWidth, gridHeight;
        if (cardWidth / gridAspectRatio <= cardHeight) {
            // Fit based on width
            gridWidth = cardWidth;
            gridHeight = gridWidth / gridAspectRatio;
        } else {
            // Fit based on height
            gridHeight = cardHeight;
            gridWidth = gridHeight * gridAspectRatio;
        }
    
        // Calculate the position to center the grid on the credit card
        const xPosition = (cardWidth - gridWidth) / 2 + offset;
        const yPosition = (cardHeight - gridHeight) / 2 + offset;
    
        // Draw bubbles directly on jsPDF canvas
        const bubbleSize = 10;
        const bubblePadding = 0.5;
    
        const bubbles = document.querySelectorAll(".bubble");
        for (let i = 0; i < bubbles.length; i++)
        {
            const row = Math.floor(i / numCols);
            const col = i % numCols;
    
            const bubbleX = xPosition + col * (bubbleSize + bubblePadding) +1;
            const bubbleY = yPosition + row * (bubbleSize + bubblePadding);
    
            // Draw text and bubblke
            const bubbleText = bubbles[i].textContent.trim();
            pdf.setFontSize(8);
            if(bubbleText == "*")
            {
                pdf.setFillColor("#188666"); // Set bubble color to green
                pdf.setTextColor("#000000"); // Set text color to black for "*"" charachter
            }
            else
            {
                pdf.setFillColor("#3498db"); // Set bubble color to blue
                pdf.setTextColor("#ffffff"); // Set text color to white for all other bubbles
            }
            pdf.circle(bubbleX + bubbleSize / 2, bubbleY + bubbleSize / 2, bubbleSize / 2, "F");
            pdf.text(bubbleX + bubbleSize / 2, bubbleY + bubbleSize / 2 +1, bubbleText, null, null, "center");
        }
    
        // add frame
        pdf.rect(offset, offset, cardWidth, cardHeight, "stroke");

        // add title
        pdf.setFontSize(16);
        pdf.setTextColor("#000000"); // Set text color to black for "*"" charachter
        const titleText = "My Passcard";
        pdf.text(titleText, offset*2, offset*2);
    
        // add seed info
        pdf.setFontSize(6);
        pdf.setTextColor("#000000"); // Set text color to black for "*"" charachter
        const seedText = "Seed: " + seedInput.value;
        pdf.text(seedText, offset + 2, cardHeight + offset - 2);
    
        // Save the PDF
        pdf.save("password_card.pdf");
    });
});

const clearButton = document.querySelector("#clearButton");
clearButton.addEventListener("click", () =>
{
    inputField.value = "";
    selectedCharacters = ""; // Clear selected characters
});


const copyButton = document.querySelector("#copyButton");
const copyMessage = document.querySelector("#copyMessage");
copyButton.addEventListener("click", copyToClipboard);

function copyToClipboard()
{
    inputField.select();
    document.execCommand("copy");

    copyMessage.style.display = "block"; // Show the message

    // Hide the message after 2 seconds
    setTimeout(() =>
    {
        copyMessage.style.display = "none";
    }, 2000);
}
